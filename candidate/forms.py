from django import forms 
from django.conf import settings
from accounts.models import User
from django.contrib.auth.forms import UserCreationForm
from tinymce.widgets import TinyMCE

from . import models

class DateInput(forms.DateInput):
    input_type = 'date'
    

class CandidateRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username','first_name', 'last_name', 'email', 'password1','password2']
    
    def save(self):
        user = super().save(commit=False)
        user.is_candidate = True 
        user.save()
        candidate = models.CandidateProfile.objects.create(user=user)
        return user


class ResumeForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 10, 'rows': 10}))

    class Meta:
        model = models.CandidateResume
        fields = ['name','email','professional_title','location','description',

                    'url_name','url_url',
                    'second_url_name','second_url_url',

                    'education_school_name','education_education_major','education_start_date',
                    'education_end_date','education_notes',

                    'second_education_school_name','second_education_education_major','second_education_start_date',
                    'second_education_end_date','second_education_notes',

                    'experience_employer','experience_job_title','experience_start_date',
                    'experience_end_date','experience_notes',

                    'second_experience_employer','second_experience_job_title','second_experience_start_date',
                    'second_experience_end_date','second_experience_notes'
                ]


        widgets =  {'education_start_date': DateInput(),
                    'education_end_date': DateInput(),
                    'experience_start_date': DateInput(),
                    'experience_end_date': DateInput(),
                    'second_experience_start_date': DateInput(),
                    'second_experience_end_date': DateInput(),
                    'second_education_start_date': DateInput(),
                    'second_education_end_date': DateInput(),
                    }
        labels = {
            'education_start_date': 'From',
            'education_end_date': 'To',
            'experience_start_date': 'From',
            'experience_end_date': 'To',
            'second_education_start_date': 'From',
            'second_education_end_date': 'To',
            'second_experience_start_date': 'From',
            'second_experience_end_date': 'To',
            'url_url': '',
            'second_url_url': ''
        }

    def __init__(self, *args, **kwargs):
        name = kwargs.pop('name', None)
        email = kwargs.pop('email', None)
        super().__init__(*args, **kwargs)

        if name:
            self.fields['name'].initial = name
        if email:
            self.fields['email'].initial = email

        self.fields['professional_title'].widget.attrs['placeholder'] = 'eg. Web Developer'
        self.fields['location'].widget.attrs['placeholder'] = 'eg. Tallinn'
        self.fields['url_name'].widget.attrs['placeholder'] = 'Name'
        self.fields['url_url'].widget.attrs['placeholder'] = 'http://'
        self.fields['url_url'].widget.attrs['class'] = 'search-field'

        self.fields['second_url_name'].widget.attrs['placeholder'] = 'Name'
        self.fields['second_url_url'].widget.attrs['placeholder'] = 'http://'
        self.fields['second_url_url'].widget.attrs['class'] = 'search-field'

        self.fields['education_school_name'].widget.attrs['placeholder'] = 'School Name'
        self.fields['education_notes'].widget.attrs['placeholder'] = 'Notes (optional)'
        self.fields['second_education_school_name'].widget.attrs['placeholder'] = 'School Name'
        self.fields['second_education_notes'].widget.attrs['placeholder'] = 'Notes (optional)'

        self.fields['experience_employer'].widget.attrs['placeholder'] = 'Employer'
        self.fields['experience_job_title'].widget.attrs['placeholder'] = 'Job Title'
        self.fields['experience_notes'].widget.attrs['placeholder'] = 'Notes (optional)'

        self.fields['second_experience_employer'].widget.attrs['placeholder'] = 'Employer'
        self.fields['second_experience_job_title'].widget.attrs['placeholder'] = 'Job Title'
        self.fields['second_experience_notes'].widget.attrs['placeholder'] = 'Notes (optional)'


#    def save(self, *args, **kwargs):


