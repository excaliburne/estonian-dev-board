# Generated by Django 2.2 on 2020-05-08 04:11

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobposts', '0003_remotejobs_closing'),
    ]

    operations = [
        migrations.AddField(
            model_name='remotejobs',
            name='source',
            field=models.URLField(default='', max_length=150),
        ),
        migrations.AlterField(
            model_name='remotejobs',
            name='closing',
            field=models.DateTimeField(default=datetime.datetime(2020, 6, 7, 4, 11, 30, 133492)),
        ),
    ]
