# Generated by Django 2.2 on 2020-05-09 07:39

from django.db import migrations, models
import employers.models


class Migration(migrations.Migration):

    dependencies = [
        ('employers', '0004_auto_20200509_0737'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='company_thumb',
            field=models.ImageField(default='default.jpg', upload_to=employers.models.upload_company_logos),
        ),
    ]
