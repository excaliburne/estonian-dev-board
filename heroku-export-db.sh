# =============================================================================== #
# =============================================================================== #
# =============================================================================== #

# copy/import from local database into heroku DB

# dump your local database into a sql file
pg_dump $DATABASE_URL > dump.sql

# import it into the heroku database
psql  $(heroku config | grep DATABASE_URL | sed 's/DATABASE_URL: //g') < dump.sql

# remove the dump
rm dump.sql