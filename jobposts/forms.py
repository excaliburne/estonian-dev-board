from django import forms
from . import models 
from tinymce.widgets import TinyMCE
from crispy_forms.helper import FormHelper


from . import models

class DateInput(forms.DateInput):
    input_type = 'date'


class JobCreateForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 10, 'rows': 10}))

    class Meta:
        model = models.Job
        fields = ('title','description','location','job_type','category',
                    'originally_posted_url','closing','salary','apply_url','years_of_exp')
        widgets = {'closing':DateInput(),
                    'category': forms.CheckboxSelectMultiple()
                }
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['location'].widget.attrs['placeholder'] = 'e.g Tallinn, Tartu'
        self.helper = FormHelper()
        self.helper.form_show_labels = False 