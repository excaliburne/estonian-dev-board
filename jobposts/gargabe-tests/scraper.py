import requests 
from bs4 import BeautifulSoup as bs

from datetime import datetime
import types

source = "https://weworkremotely.com"
url1 = 'https://weworkremotely.com/remote-europe-jobs'
page = requests.get(url1)
print(page.status_code)
soup = bs(page.text, 'lxml')
found = soup.find('div', class_='jobs-container').find_all('a')
base_date_posted = soup.find_all('span', class_='date')


def get_company_image(company_name):
    company = company_name
    url2 = "https://www.google.com/search?q="+company+"&tbm=isch&ved=2ahUKEwj2g9WbkKPpAhVUxyoKHZ7ZCTEQ2-cCegQIABAA&oq="+company+"&gs_lcp=CgNpbWcQAzICCAAyAggAMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYMgQIABAYOgQIIxAnOgQIABBDOgYIABAKEBhQ7kRYklVghl1oAHAAeACAAYEBiAGoB5IBAzUuNJgBAKABAaoBC2d3cy13aXotaW1n&sclient=img&ei=Zre0Xva4FNSOqwGes6eIAw&bih=769&biw=1488"
    page2 = requests.get(url2)
    soup2 = bs(page2.text, 'lxml')
    found2 = soup2.find_all('img')

    for i in found2[4:5]:
        image = i["src"]
    
    return image

def get_apply_link(post_link):
    link = post_link
    url3 = link
    page3 = requests.get(url3)
    soup3 = bs(page3.text, 'lxml')
    found3 = soup3.find('a', id='job-cta-alt-2')['href']
    
    return found3

def convert_date(date):
    date_splited = date.split('T')[0]
    converted = datetime.strptime(date_splited, '%Y-%m-%d')
    return converted

for i in found[2:]:
    try:
        job_title = i.find('span','title').text
        company = i.find('span','company').text
        region = i.find('span','region company').text
        job_type = i.find_all('span','company')[1].text
        company_image = get_company_image(company)
        post_link = source + i["href"]
        apply_link = get_apply_link(post_link)
        date_posted = convert_date(base_date_posted.find('time')['datetime'])

        print(job_title)
        print(date_posted)

    except:
        pass

    