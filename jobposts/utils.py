from django.db.models import Count
from . import models as models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def get_hourly(job_query):
    hourly = 0
    for hour in job_query:
        hourly = hourly + hour.salary
    hourly = hourly / 160
    hourly = str(round(hourly,2))

    return hourly

# returns a dictionary and counts the number of time categories have been used
def get_category_count(begin,limit,order=True):
    if order: queryset = models.Job.objects.values("category__id", "category__title", "category__category_thumb").annotate(Count('category__title')).order_by('-category__title__count')[begin:limit]
    else: queryset = models.Job.objects.values("category__title", "category__category_thumb")[begin:limit]
    return queryset


def pagination(request, query, paginated_by=10):
    paginator = Paginator(query, paginated_by)
    page = request.GET.get('page')
    jobs = paginator.get_page(page)

    return jobs

# check if there is given word in path and number in string
def has_numbers_and_word(word, path, inputString):
    if any(str.isdigit(c) for c in inputString) and word in path:
        return True
    else: 
        return False
    
    
   