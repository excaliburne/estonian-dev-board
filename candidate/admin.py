from django.contrib import admin
from . import models
from django.db.models import signals

class CandidateAdmin(admin.ModelAdmin):
    list_display = ('id','user','gender','language1')
    list_display_links = ('id','user')
    list_filter = ('user',)
    search_fields = ('user','gender','native_languages','city')
    list_per_page = 25


admin.site.register(models.CandidateProfile, CandidateAdmin)


# class CandidateResumeAdmin(admin.ModelAdmin):
#     list_display = ('')

admin.site.register(models.CandidateResume)

admin.site.register(models.Bookmark)
admin.site.register(models.UnBookmark)
