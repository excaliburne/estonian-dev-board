from django.db import models
from PIL import Image
from tinymce import HTMLField
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField
from django.conf import settings
from accounts.models import User
#from candidate.models import CandidateResume

from location_field.models.plain import PlainLocationField

import webcolors
from colorthief import ColorThief

# directory for company logos uploads
def upload_company_logos(instance, filename):
    return 'company_logos/{}/{}'.format(instance.user.id, filename)

class Company(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=30)
    company_description = models.CharField(max_length=300)
    company_thumb = models.ImageField(upload_to=upload_company_logos, null=False, default="default.jpg")
    phone_number = PhoneNumberField(blank=True)

    office_location = PlainLocationField(based_fields=['city'], zoom=7, default="")

    company_website = models.URLField(max_length=200, default="#")
    twitter_link = models.URLField(max_length=140, default="", null=True, blank=True)

    dominant_color_in_model = models.CharField(max_length=20, default="")

    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    is_active = models.BooleanField(default=True)

    
    class Meta:
        ordering = ('company_name',)

    def __str__(self):
        return self.company_name
    
    def save(self, *args, **kwargs):
        self.dominant_color_in_model = self.get_dominant_color()
        super(Company, self).save(*args, **kwargs)

    # convert rgb to hex code
    def rgb_to_hex(self, rgb):
        #return '%02x%02x%02x' % rgb
        try:   
            color = webcolors.rgb_to_hex(rgb)
        except:
            color = "000"
        return color
    
    # get most dominant color from company image to display in shadow box mainly
    def get_dominant_color(self):
        # alternative with color thief
        color_thief = ColorThief(self.company_thumb)
        dominant_color = color_thief.get_color(quality=1)
        palette = color_thief.get_palette(color_count=6)
        return self.rgb_to_hex(dominant_color)
    

############
#
# Bookmark a resume
#
############


# Following class can inherit from this
class BaseBookmark(models.Model):
    done_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class BookmarkManager(models.Manager):
    def find_is_bookmarked(self, resume, user):
        return self.filter(resume=resume, owner=user)
    
    def create_bookmark(self, resume, user):
        bookmark = self.create(resume=resume, owner=user)
        bookmark.save()


class ResumeBookmark(BaseBookmark):
    resume = models.ForeignKey('candidate.CandidateResume', default=1, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, default=1, on_delete=models.CASCADE) 
    is_checked = models.BooleanField(default=False)

    objects = BookmarkManager()

    def __str__(self, *args, **kwargs):
        return "{} by {}".format(self.resume.professional_title, self.resume.user.username)
        

class UnBookmarkManager(models.Manager):
    def find_is_unbookmarked(self, resume, user):
        return self.filter(resume=resume, owner=user)
    
    def delete_bookmark(self, resume, user):
        unbookmark = self.create(resume=resume, owner=user)
        unbookmark.save()


class ResumeUnBookmark(BaseBookmark):
    resume = models.ForeignKey('candidate.CandidateResume', default=1, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, default=1, on_delete=models.CASCADE) 

    objects = UnBookmarkManager()

    