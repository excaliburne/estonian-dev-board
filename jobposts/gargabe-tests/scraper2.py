import requests 
from bs4 import BeautifulSoup as bs
import datetime

url1 = 'https://weworkremotely.com/remote-europe-jobs'
page = requests.get(url1)
soup = bs(page.text, 'lxml')
found = soup.find('div', class_='jobs-container').find_all('a')

def convert_date(date):
    date_splited = date.split('T')[0]
    converted = datetime.datetime.strptime(date_splited, '%Y-%m-%d')
    return converted


for i in found:
    try:
        job_title = i.find('span','title').text
        base_date = i.find('span', class_="date")
        date = convert_date(base_date.find('time')['datetime'])
        closing = date + datetime.timedelta(days=10)

        print(date)
        print(closing)
        print(job_title)
    except:
        pass


