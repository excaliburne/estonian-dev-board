from django import template
from employers import models 
register = template.Library()

@register.simple_tag()
def is_resume_bookmarked(resume, request):
    return models.ResumeBookmark.objects.find_is_bookmarked(resume, request)

