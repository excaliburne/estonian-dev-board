from django import template
from employers import models
register = template.Library()

@register.simple_tag()
def get_dominant_color(user):
    if user:
        color = models.Company.objects.filter(user=user).values_list("dominant_color_in_model", flat=True) 
        if color:  
            for i in color:
                dominant = i
            return dominant
        else:
            return False
    return False