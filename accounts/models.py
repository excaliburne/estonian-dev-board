from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    is_candidate = models.BooleanField('candidate status', default=False)
    is_employer = models.BooleanField('employer status', default=False)
