from django.core.management.base import BaseCommand
from jobposts import models
from datetime import datetime

class Command(BaseCommand):
    help = 'Delete expired remote jobs from database'

    def add_arguments(self, parser):
        parser.add_argument('-d', dest='-d', action="store_true", help='Delete expired remote jobs')

    def handle(self, *args, **options):
        k = options.get('-d')
        if k:
            expired = models.RemoteJobs.objects.filter(closing__lt=datetime.now())
            expired.delete()
        else:
            expired = models.RemoteJobs.objects.filter(closing__lt=datetime.now())
            print(expired)
        