from jobposts import models 
from datetime import datetime

def delete_expired_remotes():
    expired = models.RemoteJobs.objects.filter(closing__lt=datetime.now())
    expired.delete()

def delete_expired_jobs():
    expired = models.Jobs.objects.filter(closing__lt=datetime.now())
    expired.delete()