from django.shortcuts import render, get_object_or_404, Http404, HttpResponseRedirect, reverse, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Post
from django.db.models import Q 
from .config import pagination

from . import models

def get_most_recent():
    queryset = Post.objects.order_by('-timestamp')[:3]
    return queryset


def index_blog(request):
    # category_count = get_category_count()

    post_list = Post.objects.all().order_by('-timestamp')

    most_recent = get_most_recent()
    featured = models.Post.objects.filter(featured=True)[:3]

    paginator = Paginator(post_list, 3)
    page_request_var = 'page'
    page = request.GET.get(page_request_var)   

    try:
        paginated_queryset = paginator.page(page)
    except PageNotAnInteger:
        paginated_queryset = paginator.page(1)
    except EmptyPage:
        paginated_queryset = paginator.page(paginator.num_pages)

    context = {
        'queryset': paginated_queryset,
        'page_request_var': page_request_var,
        'post_list': post_list,
        'most_recent':most_recent,
        'featured':featured
    }

    return render(request, 'blog.html', context)


def post_detail(request, pk):
    template_name = "blog/post_detail.html"

    post = get_object_or_404(models.Post, id=pk)

    context = {
        'post':post
    }

    return render(request, template_name, context)


def blog_search(request):
    template_name = "blog.html"
    query = request.GET.get('q')
    results = models.Post.objects.filter(Q(title__icontains=query) | Q(content__icontains=query))
    pages = pagination(request, results, num=5)

    context = {
        'queryset':pages[0],
        'page_range':pages[1]
    }

    return render(request, template_name, context)

