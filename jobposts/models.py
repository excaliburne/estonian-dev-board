from django.db import models as models
from PIL import Image
from tinymce import HTMLField
from django.utils import timezone
from employers import models as employer_models
from candidate import models as candidate_models
# from django.contrib.auth.models import User
from django.urls import reverse
from django.conf import settings
from employers.models import Company
from accounts.models import User
from datetime import datetime, timedelta

from django.utils import timezone

from urllib.parse import urlparse

# from django.utils import timezone

JOB_TYPE = (
    ('1', "Full time"),
    ('2', "Part time"),
    ('3', "Internship"),
)

YEARS_OF_EXP = (
    ('entry', 'Entry Level'),
    ('1-2', '1-2 years'),
    ('3-5', '3-5 years'),
    ('6-10', '6-10 years'),
    ('above 10', 'Above 10 years')
)


# programming language / technology / job field
class Category(models.Model):
    title = models.CharField(max_length=30)
    category_thumb = models.ImageField(upload_to="category_logos", null=True)

    class Meta:
        ordering = ('title',)

    def __str__(self):
        return self.title

class Location(models.Model):
    title = models.CharField(max_length=20)

    def __str__(self):
        return self.title


class Job(models.Model):
    title = models.CharField(max_length=70)
    description = HTMLField()
    location = models.ManyToManyField(Location)
    job_type = models.CharField(choices=JOB_TYPE, max_length=10)
    category = models.ManyToManyField(Category)
    company_name = models.ForeignKey(User, on_delete=models.CASCADE)
    originally_posted_url = models.URLField(max_length=150, default="#", null=True, blank=True)
    apply_url = models.URLField(max_length=200,default="", null=False, blank=False)
    salary = models.IntegerField(default=0, null=True, blank=True)
    years_of_exp = models.CharField('Years of Experience', max_length=20, choices=YEARS_OF_EXP, null=True, blank=True)

    start_date = models.DateField(default=datetime.now)
    closing = models.DateField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    
    featured = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    # create url post job post
    def get_absolute_url(self):
        return reverse('jobposts:job-detail', kwargs={'pk': self.pk})

    def get_bookmark_url(self, *args, **kwargs):
        return reverse('jobposts:job-bookmark', kwargs={'id': self.pk})

    def get_unbookmark_url(self, *args, **kwargs):
        return reverse('jobposts:job-unbookmark', kwargs={'id': self.pk })

    def is_active(self):
        return datetime.now() < self.closing


class RemoteJobs(models.Model):
    title = models.CharField(max_length=90)
    company = models.CharField(max_length=90)
    region = models.CharField(max_length=70)
    job_type = models.CharField(max_length=80)
    company_image = models.URLField(max_length=200)
    apply_link = models.URLField(max_length=200)
    source = models.URLField(max_length=150, default="")

    posted_at = models.DateField(default=timezone.now)
    closing = models.DateField(default=timezone.now)

    class Meta:
        unique_together = ['title','company','region','job_type','apply_link']
        ordering = ['-posted_at']

    def __str__(self):
        return self.title

    # get after http:// text
    def strip_source_url(self):
        base = urlparse(self.source).netloc
        source = '.'.join(base.split('.')[-2:])
        return source
        