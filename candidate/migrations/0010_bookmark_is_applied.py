# Generated by Django 2.2 on 2020-05-07 06:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('candidate', '0009_candidateresume_date_created'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookmark',
            name='is_applied',
            field=models.BooleanField(default=False),
        ),
    ]
