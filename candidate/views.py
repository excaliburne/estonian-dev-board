from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.urls import resolve

from jobposts import utils

from jobposts import models as job_models
from . import models


class JobBookmarkView(LoginRequiredMixin, View):
    lookup = 'id'

    def get_object(self, *args, **kwargs):
        return get_object_or_404(job_models.Job, pk=self.kwargs.get(self.lookup))
    
    def get(self, request, id=None, *args, **kwargs):
        is_bookmarked = models.Bookmark.objects.find_is_bookmarked(self.get_object(), request.user)

        if is_bookmarked.exists():
            messages.error("Post has already been bookmarked")
            return redirect('jobposts:job-list')
        
        else:
            is_unbookmarked = models.UnBookmark.objects.find_is_unbookmarked(self.get_object(), request.user)

            if is_unbookmarked.exists():
                is_unbookmarked.delete()
                models.Bookmark.objects.create_bookmark(self.get_object(), request.user)
                messages.success(request, 'Job post has been bookmarked')
                prev_link = request.META.get('HTTP_REFERER')
                prev_link_last = prev_link.split('/')[-1]
                if utils.has_numbers_and_word('job', prev_link, prev_link_last):
                    return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                else:
                    messages.success(request, 'Job post has been bookmarked')
                    return redirect('jobposts:job-list')
            
            else:
                models.Bookmark.objects.create_bookmark(self.get_object(), request.user)
                messages.success(request, 'Job post has been bookmarked')
                return redirect('jobposts:job-list')


class JobUnbookmarkView(LoginRequiredMixin, View):
    lookup = 'id'

    def get_object(self, *args, **kwargs):
        return get_object_or_404(job_models.Job, pk=self.kwargs.get(self.lookup))
    
    def get(self, request, id=None, *args, **kwargs):
        is_unbookmarked = models.UnBookmark.objects.find_is_unbookmarked(self.get_object(), request.user)
    
        if is_unbookmarked.exists():
            messages.error(request, "Post has already been unbookmarked")
            return redirect('jobposts:job-list')
        
        else:
            is_bookmarked = models.Bookmark.objects.find_is_bookmarked(self.get_object(), request.user)

            if is_bookmarked.exists():
                is_bookmarked.delete()
                models.UnBookmark.objects.delete_bookmark(self.get_object(), request.user)
                messages.success(request, 'Job post has been unbookmarked')
                if request.META.get('HTTP_REFERER'):
                    prev_link = request.META.get('HTTP_REFERER')
                    prev_link_last = prev_link.split('/')[-1]
                    if 'profile' in request.META.get('HTTP_REFERER') or utils.has_numbers_and_word('job', prev_link, prev_link_last):
                        return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                    else:
                        messages.success(request, 'Job post has been unbookmarked')
                        return redirect('jobposts:job-list')

            
            else:
                models.UnBookmark.objects.delete_bookmark(self.get_object(), request.user)
                messages.success(request, 'Job post has been unbookmarked')
                if request.META.get('HTTP_REFERER'):
                    prev_link = request.META.get('HTTP_REFERER')
                    prev_link_last = prev_link.split('/')[-1]
                    if 'profile' in request.META.get('HTTP_REFERER') or utils.has_numbers_and_word('job', prev_link, prev_link_last):
                        return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                    else:
                        return redirect('jobposts:job-list')
    