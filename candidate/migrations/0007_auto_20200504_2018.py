# Generated by Django 2.2 on 2020-05-04 20:18

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('candidate', '0006_auto_20200504_2017'),
    ]

    operations = [
        migrations.AddField(
            model_name='candidateresume',
            name='second_experience_employer',
            field=models.CharField(blank=True, max_length=30),
        ),
        migrations.AddField(
            model_name='candidateresume',
            name='second_experience_end_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now, null=True),
        ),
        migrations.AddField(
            model_name='candidateresume',
            name='second_experience_job_title',
            field=models.CharField(blank=True, max_length=60),
        ),
        migrations.AddField(
            model_name='candidateresume',
            name='second_experience_notes',
            field=models.TextField(blank=True, max_length=300),
        ),
        migrations.AddField(
            model_name='candidateresume',
            name='second_experience_start_date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now, null=True),
        ),
    ]
