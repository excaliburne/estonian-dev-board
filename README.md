# About this project

Here is a relatively big project I'm working on, mainly to train and try new things. It is not really meant for production at the moment. The big picture is done, but a lot of smaller things have not been implemented.

This Job board is "meant" for Software Engineers located in Estonia. It is nicely featured (search job post, create job post, Bookmark posts and resumes, Create resumes, etc... ) 

As said, this is a training project. I'm particulary proud of having implemented a rich search feature (box select, sort drop, etc...) in jQuery from scratch.

## Features

For both account types...
+ Register as a candidate or as an employer
+ See job posts, remote job posts and resumes
+ A scheduled aggregation of remote jobs from different websites
+ A blog

For candidates... 
+ Specific profile
+ Profile with possibility to modify usual settings (profile picture, name, email, location, etc...)
+ Manage bookmarks
+ Manage created resumes
+ Create a Resume and publish it on the site so employers can see it.

For employers...
+ Create personalized profile
+ Create job post
+ Search for resumes
+ Bookmark Resumes


## Tech used

[<img src="https://webcomicms.net/sites/default/files/clipart/175865/python-logo-png-transparent-images-175865-5846737.png
" height="70">](https://www.python.org/)
[<img src="https://static.djangoproject.com/img/logos/django-logo-negative.png" height="70">](https://www.djangoproject.com/)
[<img src="https://getbootstrap.com/docs/4.5/assets/brand/bootstrap-solid.svg" height="70">](https://getbootstrap.com/)
[<img src="https://www.postgresql.org/media/img/about/press/elephant.png" height="70">](https://www.postgresql.org/)
[<img src="https://brand.heroku.com/static/media/heroku-logotype-vertical.f7e1193f.svg" height="70">](https://dashboard.heroku.com/)
[<img src="https://www.manufacturingglobal.com/sites/default/files/styles/slider_detail/public/topic/image/21743298_1406722539365107_4308832733562613967_n.png?itok=vGaW73Lx" height="70">](https://aws.amazon.com)
[<img src="https://cdn4.iconfinder.com/data/icons/scripting-and-programming-languages/512/JQuery_logo-512.png" height="100">]("https://www.jquery.com")
[<img src="https://funthon.files.wordpress.com/2017/05/bs.png?w=772" height="70">]("https://www.crummy.com/software/BeautifulSoup/bs4/doc/")


## Snippets

#### homepage
![alt text](https://i.imgur.com/6dXMMmy.png "Homepage")

#### job post detail
![alt text](https://i.imgur.com/BwRgTIa.png "Job post detail")

More screenshots can be found here: https://imgur.com/a/rtILahx 
