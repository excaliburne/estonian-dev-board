from django.urls import path
from . import views
from candidate import views as candidate_views
from employers import views as employer_views

app_name = "jobposts"
urlpatterns = [
    path('', views.index, name="index_blog"),
    path('testvue/', views.testvue2, name="testvue"),

    #get job post detail
    path('job/<int:pk>', views.job_detail, name="job-detail"),

    # create job post
    path('job/new/', views.JobCreateView.as_view(), name="job-create"),

    # update job post
    path('job/<int:pk>/update', views.JobUpdateView.as_view(), name="job-update"),

    # delete job post
    path('job/<int:pk>/delete', views.JobDeleteView.as_view(), name="job-delete"),

    # bookmark, unbookmark job posts
    path('<int:id>/bookmark/', candidate_views.JobBookmarkView.as_view(), name="job-bookmark"),
    path('<int:id>/unbookmark/', candidate_views.JobUnbookmarkView.as_view(), name="job-unbookmark"),

    # bookmark, unbookmark resumes 
    path('resume/<int:id>/bookmark/', employer_views.ResumeBookmarkView.as_view(), name="resume-bookmark"),
    path('resume/<int:id>/unbookmark/', employer_views.ResumeUnBookmarkView.as_view(), name="resume-unbookmark"),


    # list of all job posts
    path('browse/', views.job_list, name="job-list"),
    path('categories/', views.categories, name="categories"),

    # search
    path('search/', views.search, name="search"),

    # browse resumes
    path('browse/resumes/', views.browse_resumes, name="browse-resumes"),
    # search resumes
    path('search/resumes/', views.search_resumes, name="search-resumes"),

    # browse remote jobs
    path('browse/remotes/', views.remote_job_list, name="browse-remotes")

]