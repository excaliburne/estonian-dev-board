from django import forms 
from blog.settings import AUTH_USER_MODEL as CustomUser
# from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from accounts.models import User
from crispy_forms.helper import FormHelper

from . import models





class EmployerRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1','password2']
        labels = {'username': 'Username / Company name (Single word)'}
    
    def save(self):
        user = super().save(commit=False)
        user.is_employer = True 
        user.save()
        company = models.Company.objects.create(user=user, company_name=self.cleaned_data['username'])
        return user


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['email']

class EmployerUpdateProfileForm(forms.ModelForm):
    #dominant_color_in_model = forms.CharField(widget=Color)

    class Meta:
        model = models.Company
        fields = ['company_name','company_description','company_thumb','phone_number',
                    'company_website', 'twitter_link', 'dominant_color_in_model']
        label = {'twitter_link':''}
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False 

    