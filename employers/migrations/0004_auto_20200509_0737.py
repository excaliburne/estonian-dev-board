# Generated by Django 2.2 on 2020-05-09 07:37

from django.db import migrations, models
import employers.models


class Migration(migrations.Migration):

    dependencies = [
        ('employers', '0003_company_office_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='company_thumb',
            field=models.ImageField(default='', upload_to=employers.models.upload_company_logos),
        ),
    ]
