from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from candidate import views as candidate_views
from employers import views as employer_views
from . import views

app_name = "accounts"
urlpatterns = [
    path('register/', views.register, name='register'),
    
    path('profile/', views.profile, name='profile'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),

    path('profile/settings/', views.settings, name="settings"),

    # create resume
    path('resume/new/', views.AddResumeView.as_view(), name='add-resume'),
    # access resume
    path('resume/<int:pk>', views.ResumeView.as_view(), name='resume'),
    # update resume 
    path('resume/<int:pk>/update/', views.UpdateResumeView.as_view(), name='resume-update'),
    # delete resume
    path('resume/<int:pk>/delete/', views.DeleteResumeView.as_view(), name='resume-delete'),


]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

