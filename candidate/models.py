from django.db import models
from django.conf import settings
from accounts.models import User
from datetime import datetime
from django.urls import reverse

from jobposts.models import Category, YEARS_OF_EXP, Job
from tinymce import HTMLField

from PIL import Image

import webcolors
from colorthief import ColorThief


EDUCATION_CHOICES = (
				('',''),
				('High School','High School'),
				('Vocational School', 'Vocational School'),
				('Community College','Community College'),
				("Bachelor's Degree", "Bachelor's Degree"),
				("Master's Degree", "Master's Degree"),
				('MBA', 'MBA'),
				('PhD', 'PhD'),						
			)

JOB_STATUS = (
            ('',''),
            ('Unemployed','Unemployed'),
            ('Undefined','Undefined'),
            ('Studying','Studying'),
            ('Self-employed','Self-employed'),
            ('Freelancer','Freelancer'),
            ('Employed','Employed')
)

LANGUAGES = (
            ('French','French'),
            ('English','English'),
            ('Spanish','Spanish'),
            ('Italian','Italian'),
            ('German','German'),
            ('Russian','Russian'),
            ('Swedish','Swedish'),
            ('Turkish','Turkish'),
            ('Portuguese','Portuguese'),
            ('Estonian','Estonian')
)

CITIES = (
        ('Tallinn','Tallinn'),
        ('Tartu','Tartu'),
        ('Parnu','Parnu')
)

class CandidateProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    date_of_birth = models.DateField(blank=True, null=True)
    gender = models.CharField(choices=(('male', 'Male'), ('female', 'Female'),), max_length = 10, blank=True, null=True)
    current_job_status = models.CharField(choices=JOB_STATUS, blank=True, max_length=30)
    city = models.CharField(choices=CITIES, blank=True, max_length=35)
    phone = models.CharField(max_length=24, default="")

    experience_level = models.CharField(choices=YEARS_OF_EXP, max_length=30, blank=False, default="")

    language1 = models.CharField(choices=LANGUAGES, max_length=20, default="")
    language2 = models.CharField(choices=LANGUAGES, max_length=20, blank=True)
    language3 = models.CharField(choices=LANGUAGES, max_length=20, blank=True)

    skills = models.ManyToManyField(Category)

    twitter_link = models.URLField(blank=True)
    github_link = models.URLField(blank=True)
    gitlab_link = models.URLField(blank=True)
    linkedin_link = models.URLField(blank=True)

    dominant_color_in_model = models.CharField(max_length=20, default="")

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        self.dominant_color_in_model = self.get_dominant_color()
        super(CandidateProfile, self).save(*args, **kwargs)
    
    def rgb_to_hex(self, rgb):
        try:   
            color = webcolors.rgb_to_hex(rgb)
        except:
            color = "000"

        return color
    
    def get_dominant_color(self):
        color_thief = ColorThief(self.image)
        dominant_color = color_thief.get_color(quality=1)
        palette = color_thief.get_palette(color_count=6)

        return self.rgb_to_hex(dominant_color)


class CandidateResume(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    name = models.CharField(max_length=30)
    email = models.EmailField(max_length=200)
    professional_title = models.CharField(max_length=60)
    location = models.CharField(max_length=40)
    description = HTMLField()

    # urls
    url_name = models.CharField(max_length=30, blank=True)
    url_url = models.URLField(blank=True, null=True)

    second_url_name = models.CharField(max_length=30, blank=True)
    second_url_url = models.URLField(blank=True, null=True)

    # education
    education_school_name = models.CharField(max_length=60, blank=True)
    education_education_major = models.CharField(max_length=35, blank=True, choices=EDUCATION_CHOICES)
    education_start_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    education_end_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    education_notes = models.TextField(max_length=300, blank=True)

    second_education_school_name = models.CharField(max_length=60, blank=True)
    second_education_education_major = models.CharField(max_length=35, blank=True, choices=EDUCATION_CHOICES)
    second_education_start_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    second_education_end_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    second_education_notes = models.TextField(max_length=300, blank=True)

    # experience
    experience_employer = models.CharField(max_length=30, blank=True)
    experience_job_title = models.CharField(max_length=60, blank=True)
    experience_start_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    experience_end_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    experience_notes = models.TextField(max_length=300, blank=True)

    second_experience_employer = models.CharField(max_length=30, blank=True)
    second_experience_job_title = models.CharField(max_length=60, blank=True)
    second_experience_start_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    second_experience_end_date = models.DateTimeField(default=datetime.now, null=True, blank=True)
    second_experience_notes = models.TextField(max_length=300, blank=True)

    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}\'s resume'.format(self.owner)
    
    def get_absolute_url(self):
        return reverse('accounts:resume', kwargs={'pk': self.pk})
    
    def get_bookmark_url(self, *args, **kwargs):
        return reverse('jobposts:resume-bookmark', kwargs={'id': self.pk})

    def get_unbookmark_url(self, *args, **kwargs):
        return reverse('jobposts:resume-unbookmark', kwargs={'id': self.pk })



class TestDB(models.Model):
    test = models.CharField(max_length=200)

############
#
# Bookmark a job post
#
############

# Following class can inherit from this
class BaseBookmark(models.Model):
    done_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


class BookmarkManager(models.Manager):
    def find_is_bookmarked(self, job, user):
        return self.filter(job=job, owner=user)
    
    def create_bookmark(self, job, user):
        bookmark = self.create(job=job, owner=user)
        bookmark.save()


class Bookmark(BaseBookmark):
    job = models.ForeignKey(Job, default=1, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, default=1, on_delete=models.CASCADE) 
    is_applied = models.BooleanField(default=False)

    objects = BookmarkManager()

    def __str__(self, *args, **kwargs):
        return self.job.title
        

class UnBookmarkManager(models.Manager):
    def find_is_unbookmarked(self, job, user):
        return self.filter(job=job, owner=user)
    
    def delete_bookmark(self, job, user):
        unbookmark = self.create(job=job, owner=user)
        unbookmark.save()


class UnBookmark(BaseBookmark):
    job = models.ForeignKey(Job, default=1, on_delete=models.CASCADE)
    owner = models.ForeignKey(User, default=1, on_delete=models.CASCADE) 

    objects = UnBookmarkManager()

    def __str__(self, *args, **kwargs):
        return self.job.title