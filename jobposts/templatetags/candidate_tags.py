from django import template
from candidate import models as candidate_models
register = template.Library()


@register.simple_tag()
def any_function(number):
    return number

@register.simple_tag()
def is_bookmarked(job, request):
    return candidate_models.Bookmark.objects.find_is_bookmarked(job, request)

