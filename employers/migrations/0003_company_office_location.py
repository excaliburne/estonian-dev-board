# Generated by Django 2.2 on 2020-05-05 06:31

from django.db import migrations
import location_field.models.plain


class Migration(migrations.Migration):

    dependencies = [
        ('employers', '0002_company_dominant_color_in_model'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='office_location',
            field=location_field.models.plain.PlainLocationField(default='', max_length=63),
        ),
    ]
