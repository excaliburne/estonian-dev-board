from django.apps import AppConfig


class CandidateConfig(AppConfig):
    name = 'candidate'

    def ready(self):
        import candidate.signals
