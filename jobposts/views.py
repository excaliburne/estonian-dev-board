from django.shortcuts import render, get_object_or_404, Http404, HttpResponseRedirect, reverse, redirect, HttpResponse
from django.views.generic import CreateView, UpdateView, DeleteView
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils.decorators import method_decorator
from django.db.models import Count
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import resolve
import requests
from datetime import datetime

from django.http import JsonResponse
import json

from . import models as models
from . import forms
from blogy import models as blog_models
from candidate import models as candidate_models
from . import utils
from accounts import decorators

import json
import random

###########################
#
#   TEST ONLY
#
###########################
def testvue(request):
    names = ("bob", "dan", "jack", "lizzy", "susan")
    data = list(models.Job.objects.values().order_by('-created_at'))[:4]

    items = []
    for i in range(100):
        items.append({
            "name": random.choice(names),
            "age": random.randint(20,80),
            "url": "https://example.com",
        })

    context = {
        'items_json': json.dumps(items),
        'data_json': json.dumps(data, cls=DjangoJSONEncoder),
    }

    return render(request, 'test_vue.html', context)

def testvue2(request):
    jobs = models.Job.objects.filter(is_active=True)
    
    context = {
        'jobs':jobs
    }

    return render(request, 'test_vue2.html', context)


################################
#
# END TESTS
#
################################

def index(request):
    latest = models.Job.objects.order_by('-created_at')[:7]
    queryset = models.Job.objects.all()
    count_active_jobs = models.Job.objects.filter(closing__gt=datetime.now()).count()
    count_total_jobs = models.Job.objects.all().count()
    featured = models.Job.objects.filter(featured=True).order_by('-created_at')[:3]
    latest_blog_posts = blog_models.Post.objects.all()[:3]
    categories = models.Category.objects.all()

    categories = utils.get_category_count(0,10)

    response = requests.get('https://quotesondesign.com/wp-json/wp/v2/posts/?orderby=rand')
    items = []
    for item in response.json():
        items.append({
            "quote": item['content']['rendered']
        })
        print('{}'.format(item['content']['rendered']))
    

    context = {
        'latest':latest,
        'object_list':queryset,
        'count_active_jobs':count_active_jobs,
        'count_total_jobs':count_total_jobs,
        'featured':featured,
        'latest_posts':latest_blog_posts,
        'categories':categories,
        'items':items
    }

    return render(request, 'index.html', context)

def job_detail(request, pk):
    template_name = "jobposts/job-detail.html"
    job = get_object_or_404(models.Job, id=pk)

    job_query = models.Job.objects.filter(id=pk)

    hourly = utils.get_hourly(job_query)

    context = {
        'job':job,
        'hourly':hourly
    }

    return render(request, template_name, context)


def job_list(request):
    template_name = "jobposts/browse-jobs.html"

    jobs = models.Job.objects.filter(closing__gt=datetime.now()).order_by('-created_at')
    total_full_time_jobs = jobs.filter(job_type="1").count()
    total_part_time_jobs = jobs.filter(job_type="2").count()

    total_jobs = total_full_time_jobs + total_part_time_jobs

    categories = models.Category.objects.all()

    base_jobs = models.Job.objects.filter(closing__gt=datetime.now())

    job_by_salary_desc = base_jobs.order_by('-salary')
    job_by_salary_asc = base_jobs.order_by('salary')
    job_by_oldest = base_jobs.order_by('created_at')
    job_by_expiring = base_jobs.order_by('-closing')

    value = request.POST.get('sort')
    if value in ('-salary','salary','created_at','-created_at','-expiring'):
        data = {
            'value': value,
        }
        return JsonResponse(data)
    
    value = request.POST.get('check_val')
    if value in ('check-1','check-2','check-3'):
        data = {
            'checked_value':value
        }
        return JsonResponse(data)

    jobs = utils.pagination(request, jobs, 8)
    job_by_oldest = utils.pagination(request, job_by_oldest, 8)
    job_by_salary_desc = utils.pagination(request, job_by_salary_desc, 8)
    job_by_salary_asc = utils.pagination(request, job_by_salary_asc, 8)
    job_by_expiring = utils.pagination(request, job_by_expiring, 8)

    if request.user.is_authenticated:
        context = {
            'jobs':jobs,
            'total_full_time_jobs':total_full_time_jobs,
            'total_part_time_jobs':total_part_time_jobs,
            'categories':categories,
            'job_by_salary_desc': job_by_salary_desc,
            'job_by_salary_asc': job_by_salary_asc,
            'job_by_oldest': job_by_oldest,
            'total_jobs': total_jobs,
            'job_by_expiring': job_by_expiring,
        }
        return render(request, template_name, context)
    else: 
        context = {
                'jobs':jobs,
                'total_full_time_jobs':total_full_time_jobs,
                'total_part_time_jobs':total_part_time_jobs,
                'categories':categories,
                'job_by_salary_desc': job_by_salary_desc,
                'job_by_salary_asc': job_by_salary_asc,
                'job_by_oldest': job_by_oldest,
                'total_jobs': total_jobs,
                'job_by_expiring': job_by_expiring,
            }
        return render(request, template_name, context)

    
    

@method_decorator(decorators.employer_required, name='dispatch')
class JobCreateView(LoginRequiredMixin, CreateView):
    model = models.Job
    form_class = forms.JobCreateForm
    template_name = 'jobposts/add-job_form.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            print(form.errors.as_data())
            return redirect("jobposts:job-create")

    def form_valid(self, form):
        form.instance.company_name = self.request.user
        return super().form_valid(form)



class JobUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = models.Job
    form_class = forms.JobCreateForm
    template_name = 'jobposts/add-job_form.html'

    def form_valid(self, form):
        form.instance.company_name = self.request.user
        return super().form_valid(form)
    
    # prevent users to edit any posts
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.company_name:
            return True
        return False

class JobDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = models.Job
    success_url = "/"
    template_name = "jobposts/confirm-job-delete.html"

    # prevent users to edit any posts
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.company_name:
            return True
        return False


def search(request):
    categories = models.Category.objects.all()
    jobs = models.Job.objects.filter(closing__gt=datetime.now()).order_by('-created_at')

    base_jobs = models.Job.objects.filter(closing__gt=datetime.now())

    job_by_salary_desc = base_jobs.order_by('-salary')
    job_by_salary_asc = base_jobs.order_by('salary')
    job_by_oldest = base_jobs.order_by('created_at')

    total_full_time_jobs = jobs.filter(job_type="1").count()
    total_part_time_jobs = jobs.filter(job_type="2").count()

    found_job_count = 0

    if 'keywords' in request.GET:
        keywords = request.GET['keywords']
        if keywords:
            jobs = jobs.filter(Q(title__icontains=keywords) | Q(description__icontains=keywords))

    if 'city' in request.GET:
        city = request.POST.get('city')
        if city: 
            jobs = jobs.filter(location__iexact=city)

    # checking for get request from ajax to filter categories if there is only one element in array
    if 'category_val' in request.GET:
        category_val = request.GET['category_val']

        if category_val:
            jobs = jobs.filter(category__title__iexact=category_val)
            found_job_count = jobs.count()

        # checking for get request from ajax to filter categories if there is 2 elements if array
        if 'category_val2' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            
            if category_val2:
                jobs = jobs.filter(category__title__iexact=category_val).filter(category__title__iexact=category_val2)
                found_job_count = jobs.count()

        if 'category_val3' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            category_val3 = request.GET['category_val3']
            
            if category_val3:
                jobs = jobs.filter(category__title__iexact=category_val).filter(category__title__iexact=category_val2)\
                                    .filter(category__title__iexact=category_val3)
                found_job_count = jobs.count()
        
        if 'category_val4' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            category_val3 = request.GET['category_val3']
            category_val4 = request.GET['category_val4']
            
            if category_val4:
                jobs = jobs.filter(category__title__iexact=category_val).filter(category__title__iexact=category_val2)\
                                    .filter(category__title__iexact=category_val3).filter(category__title__iexact=category_val4)
                found_job_count = jobs.count()
        
        if 'category_val5' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            category_val3 = request.GET['category_val3']
            category_val4 = request.GET['category_val4']
            category_val5 = request.GET['category_val5']
            
            if category_val5:
                jobs = jobs.filter(category__title__iexact=category_val).filter(category__title__iexact=category_val2)\
                                    .filter(category__title__iexact=category_val3).filter(category__title__iexact=category_val4)\
                                     .filter(category__title__iexact=category_val5)   
                found_job_count = jobs.count()

    paginator = Paginator(jobs, 12)
    page = request.GET.get('page')
    jobs = paginator.get_page(page)

    context = {
        'jobs': jobs,
        'values': request.GET,
        'categories': categories,
        'total_full_time_jobs': total_full_time_jobs,
        'total_part_time_jobs': total_part_time_jobs,
        'job_by_salary_desc': job_by_salary_desc,
        'job_by_salary_asc': job_by_salary_asc,
        'job_by_oldest': job_by_oldest,
        'found_job_count': found_job_count
    }

    return render(request, 'jobposts/browse-jobs.html', context)

def categories(request):
    categories1 = utils.get_category_count(0, 8)
    categories2 = utils.get_category_count(9, 17)
    categories3 = utils.get_category_count(18, 27)

    context = {
        'categories1':categories1,
        'categories2':categories2,
        'categories3':categories3,
    }

    return render(request, 'jobposts/browse-categories.html', context)


def browse_resumes(request):
    resumes = candidate_models.CandidateResume.objects.all().order_by('-date_created')
    categories = models.Category.objects.all()

    context = {
        'resumes': resumes,
        'categories': categories
    }

    return render(request, 'jobposts/browse-resumes.html', context)

def search_resumes(request):
    resumes = candidate_models.CandidateResume.objects.all().order_by('-date_created')
    categories = models.Category.objects.all()

    if 'keywords' in request.GET:
        keywords = request.GET['keywords']
        if keywords:
            resumes = resumes.filter(Q(professional_title__icontains=keywords) | Q(description__icontains=keywords) |
                                        Q(location__iexact=keywords))
    
    if 'category_val' in request.GET:
        category_val = request.GET['category_val']

        if category_val:
            resumes = resumes.filter(owner__candidateprofile__skills__title=category_val)
        
        if 'category_val2' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']

            if category_val2:
                resumes = resumes.filter(owner__candidateprofile__skills__title=category_val)\
                                    .filter(owner__candidateprofile__skills__title=category_val2)

        if 'category_val3' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            category_val3 = request.GET['category_val3']

            if category_val3:
                resumes = resumes.filter(owner__candidateprofile__skills__title=category_val)\
                                    .filter(owner__candidateprofile__skills__title=category_val2)\
                                        .filter(owner__candidateprofile__skills__title=category_val3)

        if 'category_val4' in request.GET:
            category_val = request.GET['category_val']
            category_val2 = request.GET['category_val2']
            category_val3 = request.GET['category_val3']
            category_val4 = request.GET['category_val4']

            if category_val4:
                resumes = resumes.filter(owner__candidateprofile__skills__title=category_val)\
                                    .filter(owner__candidateprofile__skills__title=category_val2)\
                                        .filter(owner__candidateprofile__skills__title=category_val3)\
                                            .filter(owner__candidateprofile__skills__title=category_val4)

    context = {
        'resumes': resumes ,
        'categories': categories
    }

    return render(request, 'jobposts/browse-resumes.html', context)

def remote_job_list(request):
    jobs = models.RemoteJobs.objects.filter(closing__gt=datetime.now())
    count_jobs_fulltime = jobs.filter(job_type="Full-Time").count()
    count_jobs_contract = jobs.filter(job_type="Contract").count()

    jobs = utils.pagination(request, jobs, 8)

    context = {
        'jobs':jobs,
        'count_jobs_fulltime':count_jobs_fulltime,
        'count_jobs_contract':count_jobs_contract
    }
    return render(request, 'jobposts/remote-jobs-list.html', context)