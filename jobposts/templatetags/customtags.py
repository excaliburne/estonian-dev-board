from django import template
from jobposts import models
register = template.Library()

@register.simple_tag()
def get_image(category_id):
    queryset = models.Category.objects.filter(id=int(category_id))
    return queryset