from django.urls import path
from . import views

app_name = "blogy"
urlpatterns = [
    path('', views.index_blog, name="index_blog"),
    path('post/<int:pk>', views.post_detail, name="post_detail"),
    path('results/', views.blog_search, name="search")
]