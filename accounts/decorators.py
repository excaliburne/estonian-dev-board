from django.contrib.auth.decorators import user_passes_test, login_required

user_candidate_required = user_passes_test(lambda user: user.is_candidate, login_url='/login/')

def candidate_required(view_func):
    decorated_view_func = login_required(user_candidate_required(view_func))
    return decorated_view_func

user_employer_required = user_passes_test(lambda user: user.is_employer, login_url='/login/')

def employer_required(view_func):
    decorated_view_func = login_required(user_employer_required(view_func))
    return decorated_view_func

    
