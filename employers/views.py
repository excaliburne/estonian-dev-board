from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from candidate import models as candidate_models
from jobposts import utils
from . import models 


class ResumeBookmarkView(LoginRequiredMixin, View):
    lookup = 'id'

    def get_object(self, *args, **kwargs):
        return get_object_or_404(candidate_models.CandidateResume, pk=self.kwargs.get(self.lookup))
    
    def get(self, request, id=None, *args, **kwargs):
        is_bookmarked = models.ResumeBookmark.objects.find_is_bookmarked(self.get_object(), request.user)

        if is_bookmarked.exists():
            messages.error("Resume has already been bookmarked")
            return redirect('jobposts:browse-resumes')
        
        else:
            is_unbookmarked = models.ResumeUnBookmark.objects.find_is_unbookmarked(self.get_object(), request.user)

            if is_unbookmarked.exists():
                is_unbookmarked.delete()
                models.ResumeBookmark.objects.create_bookmark(self.get_object(), request.user)
                if request.META.get('HTTP_REFERER'):
                    prev_link = request.META.get('HTTP_REFERER')
                    prev_link_last = prev_link.split('/')[-1]
                    if utils.has_numbers_and_word('resume', prev_link, prev_link_last):
                        return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                    else:
                        messages.success(request, 'Resume has been bookmarked')
                        return redirect('jobposts:browse-resumes')
                
            else:
                models.ResumeBookmark.objects.create_bookmark(self.get_object(), request.user)
                messages.success(request, 'Resume has been bookmarked')
                return redirect('jobposts:browse-resumes')


class ResumeUnBookmarkView(LoginRequiredMixin, View):
    lookup = 'id'

    def get_object(self, *args, **kwargs):
        return get_object_or_404(candidate_models.CandidateResume, pk=self.kwargs.get(self.lookup))
    
    def get(self, request, id=None, *args, **kwargs):
        is_unbookmarked = models.ResumeUnBookmark.objects.find_is_unbookmarked(self.get_object(), request.user)
    
        if is_unbookmarked.exists():
            messages.error(request, "Resume has already been unbookmarked")
            return redirect('jobposts:browse-resumes')
        
        else:
            is_bookmarked = models.ResumeBookmark.objects.find_is_bookmarked(self.get_object(), request.user)

            if is_bookmarked.exists():
                is_bookmarked.delete()
                models.ResumeUnBookmark.objects.delete_bookmark(self.get_object(), request.user)
                if request.META.get('HTTP_REFERER'):
                    prev_link = request.META.get('HTTP_REFERER')
                    prev_link_last = prev_link.split('/')[-1]
                    if 'profile' in request.META.get('HTTP_REFERER') or utils.has_numbers_and_word('resume', prev_link, prev_link_last):
                        return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                    else:
                        messages.success(request, 'Resume has been unbookmarked')
                        return redirect('jobposts:browse-resumes')

            
            else:
                models.ResumeUnBookmark.objects.delete_bookmark(self.get_object(), request.user)
                messages.success(request, 'Resume has been unbookmarked')
                if request.META.get('HTTP_REFERER'):
                    if 'profile' in request.META.get('HTTP_REFERER'):
                        return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
                    else:
                        return redirect('jobposts:browse-resumes')
    