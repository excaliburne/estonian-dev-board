from django.shortcuts import render, redirect, get_object_or_404, reverse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from candidate.forms import CandidateRegisterForm
from employers.forms import EmployerRegisterForm

from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import CreateView, DetailView, UpdateView, DeleteView

from candidate import models as candidate_models
from employers import models as employer_models
from jobposts import models as jobs_models

from candidate import forms as candidate_forms
from employers import forms as employers_forms

from django.utils.decorators import method_decorator
from . import decorators

from datetime import datetime


def register(request):
    if request.method =='POST':
        if 'register-candidate' in request.POST:
            form_candidate = CandidateRegisterForm(request.POST)
            form_employer = EmployerRegisterForm(request.POST)
            if form_candidate.is_valid():
                form_candidate.save()
                username = form_candidate.cleaned_data.get('username')
                messages.success(request, f'Hello { username }, your account has been created! You are now able to log in and apply for jobs!')
                return redirect('accounts:login')

        elif 'register-employer' in request.POST:
            form_employer = EmployerRegisterForm(request.POST)
            form_candidate = CandidateRegisterForm(request.POST)
            if form_employer.is_valid():
                form_employer.save()
                username = form_employer.cleaned_data.get('username')
                messages.success(request, f'Hello { username }, your account has been created! You are now able to log in and recruit peoples!')
                return redirect('accounts:login')

    else:
        form_candidate = CandidateRegisterForm()
        form_employer = EmployerRegisterForm()
        
    return render(request, 'register.html', {
                                            'form_candidate':form_candidate,
                                            'form_employer':form_employer
                                            })

@login_required
def profile(request):
    if request.user.is_candidate:
        user = request.user 
        skills = candidate_models.CandidateProfile.objects.filter(user=user)[:5]
        resumes = candidate_models.CandidateResume.objects.filter(owner=user).order_by('-date_created')
        bookmarked_jobs = candidate_models.Bookmark.objects.filter(owner=user).order_by('-done_time')

        if 'is_checked' in request.POST:
            is_checked = request.POST.get('is_checked')
            book_id = request.POST.get('book_id')
            if is_checked == "true":
                candidate_models.Bookmark.objects.filter(id=book_id).update(is_applied=True)
            if is_checked == "false":
                candidate_models.Bookmark.objects.filter(id=book_id).update(is_applied=False)

        context = {
            'user':user,
            'resumes':resumes,
            'bookmarked_jobs':bookmarked_jobs
        }
        return render(request, 'profile.html', context)
    
    elif request.user.is_employer:
        user = request.user
        jobs = jobs_models.Job.objects.filter(company_name=request.user.id).filter(closing__gt=datetime.now()).order_by('-created_at')[:3]
        bookmarked_resumes = employer_models.ResumeBookmark.objects.filter(owner=user).order_by('-done_time')

        context = {
            'user':user,
            'jobs':jobs,
            'bookmarked_resumes':bookmarked_resumes
        }
        return render(request, 'employer/profile_employer.html', context)

    else:
        return redirect('/')

@login_required
def settings(request):
    if request.user.is_employer:
        if request.method == "POST":
            u_form = employers_forms.UserUpdateForm(request.POST, instance=request.user)
            p_form = employers_forms.EmployerUpdateProfileForm(request.POST, request.FILES, instance=request.user.company)

            if u_form.is_valid() and p_form.is_valid():
                u_form.save()
                p_form.save()
                messages.success(request, 'Your account has been updated')
                return redirect('accounts:profile')
            else:
                print(u_form.errors)
                print(p_form.errors)
        else: 
            u_form = employers_forms.UserUpdateForm(instance=request.user)
            p_form = employers_forms.EmployerUpdateProfileForm(instance=request.user.company)
    
        context = {
            'u_form': u_form,
            'p_form': p_form
        }
        return render(request, 'employer/settings.html', context)


@method_decorator(decorators.candidate_required, name='dispatch')
class AddResumeView(LoginRequiredMixin, CreateView):
    model = candidate_models.CandidateResume
    form_class = candidate_forms.ResumeForm
    template_name = 'candidate/add-resume.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({
            'name': self.request.user.first_name + " " + self.request.user.last_name,
            'email': self.request.user.email
        })
        
        return kwargs

    def form_valid(self, form):
        form.instance.owner = self.request.user 
        return super().form_valid(form)


class ResumeView(DetailView):
    model = candidate_models.CandidateResume
    template_name = 'candidate/resume.html'
    context_object_name = 'resume'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


@method_decorator(decorators.candidate_required, name='dispatch')
class UpdateResumeView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = candidate_models.CandidateResume
    form_class = candidate_forms.ResumeForm
    template_name = 'candidate/update-resume.html'
    
    # prevent users to edit any resume
    def test_func(self):
        resume = self.get_object()
        if self.request.user == resume.owner:
            return True
        return False

    def form_valid(self, form):
        # print(self.request.POST)
        form.instance.owner = self.request.user
        form.save()
        return redirect(reverse("accounts:resume", kwargs={
            'pk': form.instance.pk
        }))


class DeleteResumeView(DeleteView):
    model = candidate_models.CandidateResume
    template_name = 'candidate/confirm-resume-delete.html'

    def get_success_url(self):
        return reverse('accounts:profile')

    


