from django.contrib import admin
from . import models

class CompanyAdmin(admin.ModelAdmin):
    list_filter = ('company_name','is_active','created')

admin.site.register(models.Company)